import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import IconButton from 'material-ui/IconButton';
import Tooltip from 'material-ui/Tooltip';
import DeleteIcon from 'material-ui-icons/Delete';
import Input, { InputAdornment } from 'material-ui/Input';
import Icon from 'material-ui/Icon';

import { connect } from 'react-redux';
import {
  setSearchTerm,
  removeEmployeesThunk
} from '../../redux/actionCreators/actionCreators';

const styles = theme => ({
  root: {
    paddingRight: 2
  },
  highlight:
    theme.palette.type === 'light'
      ? {
        color: theme.palette.secondary.A700,
        backgroundColor: theme.palette.secondary.A100
      }
      : {
        color: theme.palette.secondary.A100,
        backgroundColor: theme.palette.secondary.A700
      },
  spacer: {
    flex: '1 1 100%'
  },
  actions: {
    color: theme.palette.text.secondary
  },
  title: {
    flex: '0 0 auto'
  },
  textField: {
    marginRight: theme.spacing.unit
  }
});

const EmployeeTableToolbar = props => {
  const { classes, selected, removeSelected } = props;
  const numSelected = selected.length;

  return (
    <Toolbar
      className={classNames(classes.root, {
        [classes.highlight]: numSelected > 0
      })}
    >
      <div className={classes.title}>
        {numSelected > 0 ? (
          <Typography type="subheading">{numSelected} selected</Typography>
        ) : (
          <Typography type="title">Employees</Typography>
        )}
      </div>
      <div className={classes.spacer} />
      <div className={classes.actions}>
        {numSelected > 0 ? (
          <Tooltip title="Delete">
            <IconButton
              aria-label="Delete"
              onClick={() => removeSelected(selected)}
            >
              <DeleteIcon />
            </IconButton>
          </Tooltip>
        ) : (
          <Input
            id="search"
            placeholder="Search"
            type="search"
            className={classes.textField}
            margin="none"
            value={props.searchTerm}
            onChange={props.handleSearchTermChange}
            startAdornment={
              <InputAdornment position="start">
                <Icon>filter_list</Icon>
              </InputAdornment>
            }
          />
        )}
      </div>
    </Toolbar>
  );
};

EmployeeTableToolbar.propTypes = {
  classes: PropTypes.object.isRequired,
  numSelected: PropTypes.number.isRequired
};

const mapStateToProps = state => ({ ...state.tableData.toJSON() });
const mapDispatchToProps = dispatch => ({
  handleSearchTermChange(event) {
    dispatch(setSearchTerm(event.target.value));
  },
  removeSelected(ids) {
    dispatch(removeEmployeesThunk(ids));
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(styles)(EmployeeTableToolbar)
);
