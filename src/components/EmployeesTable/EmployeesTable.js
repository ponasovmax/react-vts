import React from 'react';
import { connect } from 'react-redux';
import {
  getAllEmployees,
  setSelectedId
} from '../../redux/actionCreators/actionCreators';
import _ from 'lodash';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Table, { TableBody, TableCell, TableRow } from 'material-ui/Table';
import Paper from 'material-ui/Paper';
import Checkbox from 'material-ui/Checkbox';
import Icon from 'material-ui/Icon';
import Button from 'material-ui/Button';
import AddIcon from 'material-ui-icons/Add';
import EmployeeForm from '../EmployeeForm/EmployeeForm';

import EmployeeTableToolbar from './EmployeeTableToolbar';
import EmployeeTableHeader from './EmployeeTableHeader';
import EmployeesTableFooter from './EmployeesTableFooter';
import ConfirmationDialog from '../ConfirmationDialog/ConfirmationDialog';
import { Link } from 'react-router-dom';
import { showAlert } from '../../redux/actionCreators/alertActionCreators';

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3
  },
  table: {
    minWidth: 800
  },
  tableWrapper: {
    overflowX: 'auto'
  },
  button: {
    margin: 0 // theme.spacing.unit
  }
});

export const tableColumnStyles = {
  id: {
    width: '2%'
  },
  firstName: {
    width: '20%'
  },
  lastName: {
    width: '20%'
  },
  email: {
    width: '20%'
  },
  groups: {
    width: '25%'
  },
  actions: {
    width: '10%',
    marginRight: '20px'
  }
};

class EmployeesTable extends React.Component {
  componentDidMount() {
    this.props.getAllEmployees();
  }

  isSelected = id => this.props.selected.indexOf(id) !== -1;

  buildDeleteMessage = () => {
    return `You are going to remove employee(s). This action cannot be undone. 
    Are you going to proceed?`;
  };

  render() {
    const {
      classes,
      pageData,
      data,
      emptyRows,
      dataLength,
      onSelectClick,
      showAlert
    } = this.props;
    return (
      <div>
        <Paper className={classes.root}>
          <EmployeeTableToolbar />
          <div className={classes.tableWrapper}>
            <Table className={classes.table}>
              <EmployeeTableHeader data={data} />
              <TableBody>
                {pageData.map(employee => {
                  const isSelected = this.isSelected(employee.id);
                  return (
                    <TableRow
                      hover
                      role="checkbox"
                      aria-checked={isSelected}
                      tabIndex={-1}
                      key={employee.id}
                      selected={isSelected}
                    >
                      <TableCell padding="checkbox">
                        <Checkbox
                          checked={isSelected}
                          onClick={event => onSelectClick(employee.id)}
                        />
                      </TableCell>
                      <TableCell style={tableColumnStyles.firstName}>
                        {employee.firstName}
                      </TableCell>
                      <TableCell style={tableColumnStyles.lastName}>
                        {employee.lastName}
                      </TableCell>
                      <TableCell style={tableColumnStyles.email}>
                        {employee.email}
                      </TableCell>
                      <TableCell style={tableColumnStyles.groups}>
                        {employee.groups.join()}
                      </TableCell>
                      <TableCell style={tableColumnStyles.actions}>
                        <span>
                          <Link to={`/employees/${employee.id}`}>
                            <Icon>info_outline</Icon>
                          </Link>
                          {/* <IconButton
                              className={classes.button}
                              aria-label="Edit"
                              onClick={editActionHandler.bind(
                                this,
                                employee.id
                              )}
                            >
                              <Icon>mode_edit</Icon>
                            </IconButton> */}
                        </span>
                      </TableCell>
                    </TableRow>
                  );
                })}
                {emptyRows > 0 && (
                  <TableRow style={{ height: 49 * emptyRows }}>
                    <TableCell colSpan={6} />
                  </TableRow>
                )}
              </TableBody>
              <EmployeesTableFooter dataLength={dataLength} />
            </Table>
          </div>
        </Paper>
        <ConfirmationDialog
          isOpen={false}
          title="Confirm delete action"
          message={this.buildDeleteMessage()}
        />
        <Button
          fab
          color="primary"
          aria-label="add"
          className="add-button"
          onClick={() => showAlert('An attempt to add new employee')}
        >
          <AddIcon />
        </Button>
        {/* <pre>
          <h2>PROPS</h2>
          <code>{JSON.stringify(this.props, null, 2)}</code>
          <h2>STATE</h2>
          <code>{JSON.stringify(this.state, null, 2)}</code>
        </pre> */}
      </div>
    );
  }
}

EmployeesTable.propTypes = {
  classes: PropTypes.object.isRequired
};

const formTableData = (employees, state) => {
  const {
    orderBy,
    order,
    searchTerm,
    rowsPerPage,
    page,
    selected
  } = state.tableData.toJSON();
  const filteredData = _.values(employees).filter(employee => {
    return employee.isMatch(searchTerm);
  });
  const sortedData = _.orderBy(filteredData, orderBy, order);
  const dataLength = sortedData.length;
  const pageData = sortedData.slice(
    page * rowsPerPage,
    page * rowsPerPage + rowsPerPage
  );
  const emptyRows =
    rowsPerPage - Math.min(rowsPerPage, dataLength - page * rowsPerPage);

  return {
    data: sortedData,
    dataLength,
    pageData,
    emptyRows,
    selected
  };
};

const mapStateToProps = state => ({
  ...state,
  ...formTableData(state.employees, state)
});
const mapDispatchToProps = dispatch => ({
  getAllEmployees() {
    dispatch(getAllEmployees());
  },
  onSelectClick(id) {
    dispatch(setSelectedId(id));
  },
  showAlert(message) {
    dispatch(showAlert(message));
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(styles)(EmployeesTable)
);
