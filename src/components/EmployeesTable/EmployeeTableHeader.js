import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import { connect } from 'react-redux';
import {
  setOrderField,
  setSelectedAll
} from '../../redux/actionCreators/actionCreators';
import {
  TableCell,
  TableHead,
  TableRow,
  TableSortLabel
} from 'material-ui/Table';
import Checkbox from 'material-ui/Checkbox';
import Tooltip from 'material-ui/Tooltip';
import tableColumnStyles from './EmployeesTable';

const styles = theme => ({
  table: {
    minWidth: 800
  },
  tableWrapper: {
    overflowX: 'auto'
  }
});

const columnData = [
  {
    id: 'firstName',
    numeric: false,
    disablePadding: false,
    label: 'First Name',
    sortable: true
  },
  {
    id: 'lastName',
    numeric: false,
    disablePadding: false,
    label: 'Last Name',
    sortable: true
  },
  {
    id: 'email',
    numeric: false,
    disablePadding: false,
    label: 'Email',
    sortable: true
  },
  {
    id: 'groups',
    numeric: false,
    disablePadding: false,
    label: 'Groups',
    sortable: true
  },
  { id: 'actions', numeric: false, disablePadding: false, label: 'Actions' }
];

class EmployeeTableHeader extends React.Component {
  static propTypes = {
    numSelected: PropTypes.number.isRequired,
    onRequestSort: PropTypes.func.isRequired,
    order: PropTypes.string.isRequired,
    orderBy: PropTypes.string.isRequired,
    rowCount: PropTypes.number.isRequired
  };

  render() {
    const {
      data,
      order,
      orderBy,
      selected,
      rowCount,
      onSelectAllClick
    } = this.props;
    const numSelected = selected.length;
    const ids = data.map(e => e.id);

    return (
      <TableHead>
        <TableRow>
          <TableCell padding="checkbox">
            <Checkbox
              indeterminate={numSelected > 0 && numSelected < rowCount}
              checked={numSelected === ids.length}
              onChange={() => onSelectAllClick(ids)}
            />
          </TableCell>
          {columnData.map(column => {
            const sortLabel = (
              <Tooltip
                title="Sort"
                placement={column.numeric ? 'bottom-end' : 'bottom-start'}
                enterDelay={300}
              >
                <TableSortLabel
                  active={orderBy === column.id}
                  direction={order}
                  onClick={() => this.props.setOrderField(column.id)}
                >
                  {column.label}
                </TableSortLabel>
              </Tooltip>
            );

            return (
              <TableCell
                style={tableColumnStyles[column.id]}
                key={column.id}
                numeric={column.numeric}
                padding={column.disablePadding ? 'none' : 'default'}
              >
                {column.sortable ? sortLabel : column.label}
              </TableCell>
            );
          }, this)}
        </TableRow>
      </TableHead>
    );
  }
}

const mapStateToProps = state => ({ ...state.tableData.toJSON() });
const mapDispatchToProps = dispatch => ({
  setOrderField(field) {
    dispatch(setOrderField(field));
  },
  onSelectAllClick(ids) {
    dispatch(setSelectedAll(ids));
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(styles)(EmployeeTableHeader)
);
