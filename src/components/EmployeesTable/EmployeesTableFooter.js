import React from 'react';
import { connect } from 'react-redux';
import {
  setTablePage,
  setTableRowsPerPage
} from '../../redux/actionCreators/actionCreators';
import { TableFooter, TablePagination, TableRow } from 'material-ui/Table';

class EmployeeTableFooter extends React.Component {
  render() {
    const props = this.props;
    return (
      <div>
        <TableFooter>
          <TableRow>
            <TablePagination
              count={props.dataLength}
              rowsPerPage={props.rowsPerPage}
              page={props.page}
              rowsPerPageOptions={[5, 10, 20]}
              onChangePage={props.onChangePage}
              onChangeRowsPerPage={props.onChangeRowsPerPage}
            />
          </TableRow>
        </TableFooter>
      </div>
    );
  }
}

const mapStateToProps = state => ({ ...state.tableData.toJSON() });
const mapDispatchToProps = (dispatch, ownProps) => ({
  onChangePage(event, page) {
    dispatch(setTablePage(page));
  },
  onChangeRowsPerPage(event) {
    dispatch(setTableRowsPerPage(event.target.value));
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(
  EmployeeTableFooter
);
