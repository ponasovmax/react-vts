import React from 'react';
import Button from 'material-ui/Button';
import TextField from 'material-ui/TextField';
import { withStyles } from 'material-ui/styles';
import Paper from 'material-ui/Paper';
import { Row, Col } from 'react-flexbox-grid';
import { Link } from 'react-router-dom';

const styles = theme => ({
  root: theme.mixins.gutters({
    paddingTop: 16,
    paddingBottom: 16,
    marginTop: theme.spacing.unit * 3
  }),
  button: {
    margin: theme.spacing.unit * 3
  }
});

class LoginForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: ''
    };
  }

  localLoginHandler = () => {
    const { loginHandler } = this.props;
    const { username, password } = this.state;
    loginHandler(username, password, true);
  };

  onChangeHandler = (event, newValue) => {
    const { name, value } = event.target;
    this.setState({ ...this.state, [name]: value });
  };

  render() {
    const { classes } = this.props;
    return (
      <Row center="xs">
        <Col xs={4}>
          <Paper className={classes.root} elevation={4}>
            <TextField
              label="Username"
              name="username"
              value={this.state.username}
              onChange={this.onChangeHandler}
              margin="normal"
            />
            <br />
            <TextField
              type="password"
              name="password"
              label="Password"
              value={this.state.password}
              onChange={this.onChangeHandler}
              margin="normal"
            />
            <br />
            <Link to="/">
              <Button
                className={classes.button}
                raised
                color="primary"
                onClick={this.localLoginHandler}
              >
                Login
              </Button>
            </Link>
          </Paper>
        </Col>
      </Row>
    );
  }
}

export default withStyles(styles)(LoginForm);
