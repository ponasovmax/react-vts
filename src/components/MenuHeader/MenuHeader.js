import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';
import messages from 'assets/messages';
import { Link } from 'react-router-dom';

const styles = {
  flex: {
    flex: 1
  }
};

class MenuHeader extends React.Component {
  buttonClickHandler = () => {
    const { user, loginHandler } = this.props;
    const newAuth = !user.auth;
    const username = newAuth ? user.username : '';
    const password = newAuth ? user.password : '';
    loginHandler(username, password, newAuth);
  };

  render() {
    const { classes } = this.props;
    const { user } = this.props;
    const { username, auth } = user;

    const actionButton = auth ? (
      <Button onClick={this.buttonClickHandler} color="contrast">
        Logout
      </Button>
    ) : (
      <Button color="contrast">Login</Button>
    );

    return (
      <AppBar position="static">
        <Toolbar>
          <Typography type="title" color="inherit" className={classes.flex}>
            {messages.labels.appTitle}
          </Typography>
          <Typography type="body1" color="inherit" align="right">
            {auth ? `Hi, ${username}` : ''}
          </Typography>
          <Link to="/login">{actionButton}</Link>
        </Toolbar>
      </AppBar>
    );
  }
}

MenuHeader.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(MenuHeader);
