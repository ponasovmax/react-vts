import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import TextField from 'material-ui/TextField';
import Button from 'material-ui/Button';
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle
} from 'material-ui/Dialog';

import Employee from 'model/employee';
import ChipInput from 'material-ui-chip-input';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  },
  menu: {
    width: 200
  }
});

const initialState = {
  firstName: false,
  lastName: false,
  email: false
};

class EmployeeForm extends Component {
  state = {
    touched: { ...initialState }
  };

  needToShowError = (handler, object, key) => {
    const val = object[key];
    return !this.state.touched[key] || handler(val);
  };

  changeWrapper = e => {
    const fieldTouchStatus = { ...this.state.touched, [e.target.name]: true };
    this.setState({
      touched: fieldTouchStatus
    });
    this.props.changePropHandler(e);
  };

  resetFormState = () => {
    this.setState({
      touched: { ...initialState }
    });
  };

  closeWrapper = () => {
    this.resetFormState();
    this.props.formCloseHandler();
  };

  saveWrapper = employee => {
    if (!employee.isValid()) {
      this.setState({
        touched: { firstName: true, lastName: true, email: true }
      });
    } else {
      this.resetFormState();
      this.props.saveHandler();
    }
  };

  render() {
    const {
      classes,
      isFormShown,
      formCloseHandler,
      employee,
      groupsHandlers
    } = this.props;
    const form = {
      title: employee.id ? 'Edit Employee' : 'Add Employee',
      message:
        (employee.id ? 'To update existing' : 'To add a new') +
        ' employee you have to fill in fields below.',
      buttonLabel: employee.id ? 'Update' : 'Save'
    };

    const firstNameHasError = !this.needToShowError(
      Employee.validateName,
      employee,
      'firstName'
    );
    const lastNameHasError = !this.needToShowError(
      Employee.validateName,
      employee,
      'lastName'
    );
    const emailHasError = !this.needToShowError(
      Employee.validateEmail,
      employee,
      'email'
    );

    const validation = {
      firstName: {
        error: firstNameHasError,
        errorMessage: firstNameHasError ? 'This name is invalid' : ''
      },
      lastName: {
        error: lastNameHasError,
        errorMessage: lastNameHasError ? 'This name is invalid' : ''
      },
      email: {
        error: emailHasError,
        errorMessage: emailHasError ? 'This email is invalid' : ''
      }
    };

    return (
      <Dialog open={isFormShown} onRequestClose={formCloseHandler}>
        <DialogTitle>{form.title}</DialogTitle>
        <DialogContent>
          <DialogContentText>{form.message}</DialogContentText>
          <div className={classes.container}>
            <TextField
              id="firstName"
              name="firstName"
              label="First Name"
              autoFocus
              className={classes.textField}
              value={employee.firstName}
              onChange={this.changeWrapper}
              error={validation.firstName.error}
              helperText={validation.firstName.errorMessage}
              margin="normal"
              fullWidth
            />
            <TextField
              id="lastName"
              name="lastName"
              label="Last Name"
              className={classes.textField}
              value={employee.lastName}
              onChange={this.changeWrapper}
              error={validation.lastName.error}
              helperText={validation.lastName.errorMessage}
              margin="normal"
              fullWidth
            />
            <TextField
              id="email"
              name="email"
              label="Email"
              value={employee.email}
              className={classes.textField}
              onChange={this.changeWrapper}
              error={validation.email.error}
              helperText={validation.email.errorMessage}
              margin="normal"
              fullWidth
            />
            <ChipInput
              id="groups"
              name="groups"
              label="Groups"
              margin="normal"
              className={classes.textField}
              value={employee.groups}
              onRequestAdd={groupsHandlers.add}
              onRequestDelete={groupsHandlers.remove}
              fullWidth
            />
          </div>
        </DialogContent>
        <DialogActions>
          <Button onClick={this.closeWrapper} color="primary">
            Cancel
          </Button>
          <Button onClick={() => this.saveWrapper(employee)} color="primary">
            {form.buttonLabel}
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

EmployeeForm.propTypes = {
  isFormShown: PropTypes.bool.isRequired
};


const mapStateToProps = state => ({ ...state.employeeDetails });
const mapDispatchToProps = (dispatch, ownProps) => ({
  getAllEmployees() {
    dispatch();
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(EmployeeForm));
