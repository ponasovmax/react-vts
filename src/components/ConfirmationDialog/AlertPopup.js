import React from 'react';
import Snackbar from 'material-ui/Snackbar';
import IconButton from 'material-ui/IconButton';
import CloseIcon from 'material-ui-icons/Close';
import { withStyles } from 'material-ui/styles';
import { connect } from 'react-redux';

import { hideAlert } from '../../redux/actionCreators/alertActionCreators';

const AlertPopup = props => {
  const { open, message, hideAlert } = props;
  return (
    <div>
      <Snackbar
        open={open}
        message={message}
        autoHideDuration={5000}
        onRequestClose={() => hideAlert()}
        action={[
          <IconButton
            key="close"
            aria-label="Close"
            color="accent"
            onClick={() => hideAlert()}
          >
            <CloseIcon />
          </IconButton>
        ]}
      />
    </div>
  );
};

const mapStateToProps = state => ({ ...state.alert.toJSON() });
const mapDispatchToProps = dispatch => ({
  hideAlert() {
    dispatch(hideAlert());
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles()(AlertPopup)
);
