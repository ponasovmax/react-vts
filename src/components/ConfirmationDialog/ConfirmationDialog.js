import React from 'react';
import Button from 'material-ui/Button';
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle
} from 'material-ui/Dialog';

const ConfirmationDialog = props => {
  const { isOpen, title, message, handleDecline, handleApprove } = props;
  return (
    <Dialog open={isOpen} onRequestClose={handleDecline}>
      <DialogTitle>{title}</DialogTitle>
      <DialogContent>
        <DialogContentText>{message}</DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleDecline} color="primary">
          Disagree
        </Button>
        <Button onClick={handleApprove} color="primary" autoFocus>
          Agree
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default ConfirmationDialog;
