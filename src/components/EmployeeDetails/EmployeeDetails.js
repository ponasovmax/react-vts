import React from 'react';
import { connect } from 'react-redux';
import {
  getEmployeeDetails,
  setCurrentEmployee
} from '../../redux/actionCreators/employeeDetailsActionCreators';
import { withStyles } from 'material-ui/styles';
import Paper from 'material-ui/Paper';
import { CircularProgress } from 'material-ui/Progress';
import Typography from 'material-ui/Typography';
import { Row, Col } from 'react-flexbox-grid';

const styles = theme => ({
  root: theme.mixins.gutters({
    paddingTop: 16,
    paddingBottom: 16,
    marginTop: theme.spacing.unit * 3
  }),
  typography: {
    margin: theme.spacing.unit * 3
  }
});

class EmployeeDetails extends React.Component {
  componentDidMount() {
    const employeeId = this.props.match.params.id;
    const user = this.props.data[employeeId];
    if (user) {
      this.props.setCurrentEmployee(user);
    } else {
      this.props.getEmployeeDetails(employeeId);
    }
  }

  render() {
    const { classes, currentEmployee } = this.props;

    const details = currentEmployee ? (
      <div>
        <Typography type="display1" align="center" component="h3">
          Employee Details:
        </Typography>
        <Typography className={classes.typography} type="headline">
          Name : {currentEmployee.fullName}
        </Typography>
        <Typography className={classes.typography} type="headline">
          Email : {currentEmployee.email}
        </Typography>
        <Typography className={classes.typography} type="headline">
          Groups :{' '}
          {currentEmployee.groups ? currentEmployee.groups.join(',') : ''}
        </Typography>
      </div>
    ) : (
      <CircularProgress size={50} center />
    );

    return (
      <div>
        <Row className="Details" center="xs">
          <Col xs={6}>
            <Paper className={classes.root} elevation={4}>
              {details}
            </Paper>
          </Col>
        </Row>
        <pre>
          <h2>PROPS</h2>
          <code>{JSON.stringify(this.props, null, 2)}</code>
          <h2>STATE</h2>
          <code>{JSON.stringify(this.state, null, 2)}</code>
        </pre>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  data: state.employeeDetails.get('data'),
  currentEmployee: state.employeeDetails.get('current')
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  getEmployeeDetails(id) {
    dispatch(getEmployeeDetails(id));
  },
  setCurrentEmployee(employee) {
    dispatch(setCurrentEmployee(employee));
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(styles)(EmployeeDetails)
);
