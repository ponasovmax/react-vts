import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';

import { Grid } from 'react-flexbox-grid';
import { Switch, Route } from 'react-router-dom';
import './App.css';
import MenuHeader from '../components/MenuHeader/MenuHeader';
import LoginForm from '../components/LoginForm/LoginForm';
import EmployeeDetails from '../components/EmployeeDetails/EmployeeDetails';
import EmployeesTable from '../components/EmployeesTable/EmployeesTable';
import AlertPopup from '../components/ConfirmationDialog/AlertPopup';

class App extends React.Component {
  EmployeeDetails = props => (
    <EmployeeDetails {...props} employee={this.props.currentEmployee} />
  );

  render() {
    return (
      <main>
        <MenuHeader user={{}} loginHandler={this.loginHandler} />
        <Grid fluid className="App">
          <Switch>
            <Route exact path="/">
              <EmployeesTable />
            </Route>
            <Route path="/login">
              <LoginForm loginHandler={this.loginHandler} />
            </Route>
            <Route path="/employees/:id" component={this.EmployeeDetails} />
          </Switch>
        </Grid>
        <AlertPopup />
      </main>
    );
  }
}

App.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles()(App);
