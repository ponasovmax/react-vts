/* eslint-disable   no-useless-escape*/

import uuidv4 from 'uuid/v4';
require('datejs');

class Employee {
  constructor(props = {}) {
    this._id = props.id || '';
    this._firstName = props.firstName || '';
    this._lastName = props.lastName || '';
    this._email = props.email || '';
    this._groups = props.groups || [];
    this._manager = props.manager;
    this._startDate = props.startDate;
    this._balance = props.balance;
  }

  setProp = (key, value) => {
    switch (key) {
      case 'firstName':
        this._firstName = value;
        break;
      case 'lastName':
        this._lastName = value;
        break;
      case 'email':
        this._email = value;
        break;
      case 'groups':
        this._groups = value;
        break;
      case 'manager':
        this._manager = value;
        break;
      case 'startDate':
        this._startDate = value;
        break;
      case 'balance':
        this._balance = value;
        break;
      default:
        console.error('An attempt ot set value for unsupported key', key);
    }
  };

  get id() {
    return this._id;
  }
  get firstName() {
    return this._firstName;
  }
  get lastName() {
    return this._lastName;
  }
  get fullName() {
    return `${this._firstName} ${this._lastName}`;
  }
  get email() {
    return this._email;
  }
  get groups() {
    return this._groups;
  }
  get groupsFormatted() {
    return this._groups.sort().reduce((s, g) => (s += `${g} `), '');
  }
  get manager() {
    return this._manager;
  }
  get startDate() {
    return this._startDate;
  }
  get startDateFormatted() {
    return new Date(this._startDate).toString('dd MMM yyyy');
  }
  get balance() {
    return this._balance;
  }
  get balanceFormatted() {
    if (!this._balance) {
      return '0 Days';
    }
    const days = Math.floor(this._balance / 8);
    const hours = this._balance % 8;
    return hours ? `${days} Days ${hours} Hours` : `${days} Days`;
  }
  get obj() {
    return {
      id: this.id,
      firstName: this.firstName,
      lastName: this.lastName,
      email: this.email,
      groups: this.groups,
      manager: this.manager,
      startDate: this.startDate,
      balance: this.balance
    };
  }

  isMatch = query =>
    `${this.fullName} ${this._email}`.toLowerCase().includes(query);

  isValid = () =>
    Employee.validateName(this._firstName) &&
    Employee.validateName(this._lastName) &&
    Employee.validateEmail(this._email) &&
    Employee.validateGroups(this._groups);

  generateId = () => {
    this._id = this._id || uuidv4();
    return this._id;
  };

  static validateProp = (propName, value) => {
    switch (propName) {
      case 'firstName':
      case 'lastName':
        return Employee.validateName(value);
      case 'email':
        return Employee.validateEmail(value);
      case 'groups':
        return Employee.validateGroups(value);
      default:
        return true;
    }
  };

  static validateName = name => /^[a-zA-Z ]+$/.test(name);

  static validateEmail = email =>
    /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(email);

  static validateGroups = groups =>
    groups.reduce((res, group) => Employee.validateGroup(group) && res, true);

  static validateGroup = group => /^[\w.-]+$/.test(group);
}

export default Employee;
