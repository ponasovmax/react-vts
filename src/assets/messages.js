const messages = {
  labels: {
    appTitle : "Helmes Vacation Tracking System"
  },
  warnings: {
    alreadyExists: "Such user already exists",
    editUnexistingUser: "An attempt to edit unexisting user",
    employeeUpdated: "Employee data was successfully updated",
    employeeCreated: "New employee was successfully created"
  },
  errors: {
    serverError: "The server error occurred"
  }
};

export default messages;