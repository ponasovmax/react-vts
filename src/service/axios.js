import axios from 'axios';
import _ from 'lodash';
import Employee from './../model/employee';

const axiosClient = axios.create({
  baseURL: '/api/employees'
});

class RestClient {
  getAll = () => {
    return axiosClient
      .get()
      .then(resp =>
        _.reduce(
          resp.data,
          (hash, employee) => {
            hash[employee.id] = new Employee(employee);
            return hash;
          },
          {}
        )
      )
      .catch(error => console.error(error));
  };

  getOne = id => {
    return axiosClient.get(id).then(resp => new Employee(resp.data));
  };

  create = employee => {
    return axiosClient.post('', employee);
  };

  update = employee => {
    return axiosClient.put(employee.id, employee);
  };

  remove = id => {
    return axiosClient.delete(id);
  };
}

export default RestClient;
