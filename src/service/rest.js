import Rest from "fetch-on-rest";
import Employee from "./../model/employee";

const api = new Rest("/api/employees");

class RestClient {
  getAll = handler => {
    return api
      .get()
      .then(resp => resp.map(e => new Employee(e)))
      .catch(e => console.error(e))
      .then(data => handler(data));
  };

  getOne = (id, handler) => {
    console.log(id);
    return api
      .get(id)
      .then(resp => new Employee(resp))
      .catch(e => console.error(e))
      .then(e => handler(e));
  };

  create = (body, handler) => {
    return api
      .post("", body)
      .catch(e => console.console.error(e))
      .then(data => handler(data));
  };

  update = (id, body, handler) => {
    return api
      .put([id], body)
      .catch(e => console.console.error(e))
      .then(data => handler(data));
  };

  remove = (id, handler) => {
    return api
      .del([id])
      .catch(e => console.logconsole.error(e))
      .then(data => handler(data));
  };
}

export default RestClient;
