import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import reducer from './reducers/reducers';
import { createLogger } from 'redux-logger';
import promiseMiddleware from 'redux-promise-middleware';

const store = createStore(
  reducer,
  applyMiddleware(promiseMiddleware(), thunk, createLogger())
);

export default store;
