import { alertActions } from '../actions/alertActions';

export const showAlert = message => {
  return { type: alertActions.SHOW_ALERT, payload: message };
};

export const hideAlert = () => {
  return { type: alertActions.HIDE_ALERT };
};
