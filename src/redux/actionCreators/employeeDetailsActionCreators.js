import { employeeDetailsActions } from '../actions/employeeDetailsActions';
import RestClient from '../../service/axios';

const client = new RestClient();

export function setCurrentEmployee(employee) {
  return {
    type: employeeDetailsActions.SET_CURRENT_EMPLOYEE,
    payload: employee
  };
}

export function getEmployeeDetails(id) {
  return dispatch => {
    client
      .getOne(id)
      .then(employee => dispatch(setCurrentEmployee(employee)))
      .catch(error => {
        console.error('axios error', error);
      });
  };
}

export function cleanCurrentEmployee(employee) {
  return { type: employeeDetailsActions.CLEAN_CURRENT_EMPLOYEE };
}

export function openCreateForm() {
  return { type: employeeDetailsActions.OPEN_CREATE_FORM };
}

export function openEditForm(employee) {
  return { type: employeeDetailsActions.OPEN_CREATE_FORM, payload: employee };
}

export function hideForm(employee) {
  return { type: employeeDetailsActions.HIDE_FORM };
}

export function setEmployeeProp(key, value) {
  return { type: employeeDetailsActions.SET_EMPLOYEE_PROPERTY, key, value };
}
