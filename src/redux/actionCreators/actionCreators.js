import {
  employeesActions,
  tableDataActions
} from '../actions/actions';
import RestClient from '../../service/axios';

const client = new RestClient();

export function setEmployees(employees) {
  return { type: employeesActions.SET_EMPLOYEES, payload: employees };
}

export function getAllEmployees() {
  return dispatch => {
    client
      .getAll()
      .then(response => {
        dispatch(setEmployees(response));
      })
      .catch(error => {
        console.error('axios error', error);
      });
  };
}

export function setEmployee(employee) {
  return { type: employeesActions.SET_EMPLOYEE, payload: employee };
}

export function createEmployee(employee) {
  return { type: employeesActions.CREATE_EMPLOYEE, payload: employee };
}

export function createEmployeeThunk(employee) {
  return dispatch => {
    client
      .create(employee)
      .then(() => {
        dispatch(createEmployee(employee));
      })
      .catch(error => {
        console.error('axios error', error);
      });
  };
}

export function updateEmployee(employee) {
  return { type: employeesActions.UPDATE_EMPLOYEE, payload: employee };
}

export function updateEmployeeThunk(employee) {
  return dispatch => {
    client
      .update(employee)
      .then(() => {
        dispatch(updateEmployee(employee));
      })
      .catch(error => {
        console.error('axios error', error);
      });
  };
}

export function removeEmployee(id) {
  return { type: employeesActions.REMOVE_EMPLOYEE, payload: id };
}

export function removeEmployeeThunk(id) {
  return dispatch => {
    client
      .remove(id)
      .then(() => {
        dispatch(removeEmployee(id));
      })
      .catch(error => {
        console.error('axios error', error);
      });
  };
}

export function removeEmployees(ids) {
  return { type: employeesActions.REMOVE_EMPLOYEES, payload: ids };
}

export function removeEmployeesThunk(ids) {
  return dispatch => {
    dispatch(cleanSelected());
    client
      .remove(ids.join(','))
      .then(() => {
        dispatch(removeEmployees(ids));
      })
      .catch(error => {
        console.error('axios error', error);
      });
  };
}

export function setTablePage(page) {
  return {
    type: tableDataActions.SET_TABLE_PAGE,
    payload: page
  };
}

export function setTableEmployees(employees) {
  return {
    type: tableDataActions.SET_TABLE_EMPLOYEES,
    payload: employees
  };
}

export function setTableRowsPerPage(rowsCount) {
  return {
    type: tableDataActions.SET_TABLE_ROWS_PER_PAGE,
    payload: rowsCount
  };
}

export function setSearchTerm(searchTerm) {
  return {
    type: tableDataActions.SET_SEARCH_TERM,
    payload: searchTerm
  };
}

export function setOrderField(field) {
  return {
    type: tableDataActions.SET_TABLE_ORDER_FIELD,
    payload: field
  };
}

export function setSelectedId(id) {
  return {
    type: tableDataActions.HANDLE_SELECTED,
    payload: id
  };
}

export function setSelectedAll(ids) {
  return {
    type: tableDataActions.HANDLE_BULK_SELECTED,
    payload: ids
  };
}

export function cleanSelected() {
  return {
    type: tableDataActions.CLEAN_SELECTED
  };
}
