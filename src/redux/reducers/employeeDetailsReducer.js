import { employeeDetailsActions } from '../actions/employeeDetailsActions';
import { Map } from 'immutable';
import Employee from '../../model/employee';

const initialState = new Map({
  data: {},
  isFormShown: false,
  action: 'create',
  current: new Employee()
});

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case employeeDetailsActions.SET_EMPLOYEE_PROPERTY:
      return setEmployeeProperty(state, action);
    case employeeDetailsActions.SET_CURRENT_EMPLOYEE:
      return state.set('current', action.payload);
    case employeeDetailsActions.CLEAN_CURRENT_EMPLOYEE:
      return state.set('current', new Employee());
    case employeeDetailsActions.OPEN_CREATE_FORM:
      return setFormState(state, action, 'create');
    case employeeDetailsActions.OPEN_UPDATE_FORM:
      return setFormState(state, action, 'update');
    case employeeDetailsActions.HIDE_FORM:
      return state.set('isFormShown', false);
    default:
      return state;
  }
};

const setFormState = (state, action, formAction) => {
  return state
    .set('isFormShown', true)
    .set('action', formAction)
    .set('current', action.payload || new Employee());
};

const setEmployeeProperty = (state, action) => {
  const currentEmployee = state.get('current') || new Employee();
  currentEmployee.setProp(action.key, action.value);
  return state.set('current', currentEmployee);
};

export default reducer;
