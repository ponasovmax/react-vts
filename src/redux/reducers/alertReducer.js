import { Map } from 'immutable';
import { alertActions } from '../actions/alertActions';

const initialState = new Map({
  message: '',
  open: false
});

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case alertActions.SHOW_ALERT:
      return state.set('message', action.payload).set('open', true);
    case alertActions.HIDE_ALERT:
      return initialState;
    default:
      return state;
  }
};

export default reducer;
