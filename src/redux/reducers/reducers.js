import { combineReducers } from 'redux';
import { Map, Set } from 'immutable';
import alert from './alertReducer';
import employeeDetails from './employeeDetailsReducer';
import {
  employeesActions,
  tableDataActions
} from '../actions/actions';
import _ from 'lodash';

const employees = (state = {}, action) => {
  switch (action.type) {
    case employeesActions.SET_EMPLOYEES:
      return action.payload;
    case employeesActions.CREATE_EMPLOYEE:
      action.payload.generateId();
      return { ...state, [action.payload.id]: action.payload };
    case employeesActions.UPDATE_EMPLOYEE:
      return { ...state, [action.payload.id]: action.payload };
    case employeesActions.REMOVE_EMPLOYEE:
      return _.omit(state, [action.payload]);
    case employeesActions.REMOVE_EMPLOYEES:
      return _.omit(state, action.payload);
    default:
      return state;
  }
};

const initialTableState = new Map({
  searchTerm: '',
  order: 'asc',
  orderBy: 'id',
  selected: new Set(),
  page: 0,
  rowsPerPage: 5
});

const tableData = (state = initialTableState, action) => {
  switch (action.type) {
    case tableDataActions.SET_TABLE_PAGE:
      return state.set('page', action.payload);
    case tableDataActions.SET_TABLE_ROWS_PER_PAGE:
      return state.set('rowsPerPage', action.payload);
    case tableDataActions.SET_SEARCH_TERM:
      return state.set('searchTerm', action.payload);
    case tableDataActions.SET_TABLE_ORDER_FIELD:
      return updateOrderState(state, action);
    case tableDataActions.HANDLE_SELECTED:
      return updateSelectedId(state, action.payload);
    case tableDataActions.HANDLE_BULK_SELECTED:
      return updateSelectedIds(state, action.payload);
    case tableDataActions.CLEAN_SELECTED:
      return cleanSelected(state);
    default:
      return state;
  }
};

const updateOrderState = (state, action) => {
  if (state.get('orderBy') === action.payload) {
    const newOrder = state.get('order') === 'asc' ? 'desc' : 'asc';
    return state.set('order', newOrder);
  } else {
    return state.set('orderBy', action.payload);
  }
};

const updateSelectedId = (state, id) => {
  const selected = state.get('selected');
  return state.set(
    'selected',
    selected.has(id) ? selected.delete(id) : selected.add(id)
  );
};

const updateSelectedIds = (state, ids) => {
  const selected = state.get('selected');
  return state.set(
    'selected',
    selected.size < ids.length ? new Set(ids) : new Set()
  );
};

const cleanSelected = (state, ids) => {
  return state.set('selected', new Set());
};

const rootReducer = combineReducers({
  employees,
  employeeDetails,
  tableData,
  alert
});

export default rootReducer;
